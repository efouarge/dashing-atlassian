require 'dotenv'
require 'jira'

require './settings.rb'

Dotenv.load

options = {
  username: ENV['JIRA_USERNAME'],
  password: ENV['JIRA_PASSWORD'],
  site: Potherca::Dashing.settings[:jira][:url],
  context_path: '',
  auth_type: :basic,
  use_ssl: true
}

# @CHECKME: When credentials fail, does this crash or not until the JQL call?
#           If here, `rescue` needs to be added!
client = JIRA::Client.new(options)

query_type = 'issues_in_sprint'

if query_type == 'issues_in_sprint'
  query = 'status != Resolved AND Sprint in openSprints()'
  title = 'Issue Count'
  sub_title = 'Issues in Current Sprint'
else
  project = 'SURVEY'
  query = "project = #{project} and status != Resolved"
  title = "#{project} Issue Count"
  sub_title = 'Unresolved Issues'
end

SCHEDULER.every '10m', first_in: 0 do |_job|
  begin
    query_options = {
      fields: [],
      start_at: 0,
      max_results: 100_000
    }
    issues = client.Issue.jql(query, query_options)
  rescue JIRA::HTTPError => e
    puts "Jira HTTP Error: #{e.response.code} - #{e.response.message}"
    # puts e.response.body

    send_event('jira-issue-count',
               current: e.response.code,
               moreinfo: e.response.message,
               title: 'Jira Issue Count - Error!',
               status: 'warning'
    )
  end

  if issues
    issue_count = issues.length
    send_event('jira-issue-count',
               current: issue_count,
               moreinfo: sub_title,
               title: title
    )
  end
end
