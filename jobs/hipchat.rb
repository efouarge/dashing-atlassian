require 'dotenv'
require 'json'
require 'hipchat'

require './settings.rb'

Dotenv.load

api_token = ENV['HIPCHAT_TOKEN']
room = Potherca::Dashing.settings[:hipchat][:room]

client = HipChat::Client.new(api_token, api_version: 'v2')

# Get history for a date in time with a particular timezone
#                     (default is latest 75 messages, timezone default is 'UTC')
# history = client['my room'].history(:date => '2015-02-10', :timezone => 'CET')

# ------------------------------------------------------------------------------
SCHEDULER.every '10m', first_in: 0 do |_job|
  begin
    history_json = client[room].history
    history = JSON.parse(history_json)
  rescue JIRA::HTTPError => e
    puts "HipChat HTTP Error: #{e.response.code} - #{e.response.message}"
    # puts e.response.body

    send_event('hipchat-room-history',
               current: e.response.code,
               moreinfo: e.response.message,
               title: 'HipChat Room History - Error!',
               status: 'warning'
    )
  end

  if history
    hipchat_comments = []

    history['items'].each do |item|
      # @TODO: Keep track of last `item['date']` and only fetch from there
      message_time = DateTime.parse(item['date']).strftime('%F %T')
      message_mentions = item['mentions']

      message = "#{message_time} "
      if message_mentions.length > 0
        message += ' (to'
        message_mentions.each do |mention|
          message += " #{mention['name']}"
          message += ') '
        end
      end
      message += ":\n\t#{item['message']}"

      # @TODO: Get list of user's avatars
      # avatar: ,
      hipchat_comments << { name: item['from']['name'], body: message }
    end
    send_event('hipchat-room-history',
               comments: hipchat_comments,
               moreinfo: '',
               title: "HipChat - #{room}"
    )
  end
end
# ------------------------------------------------------------------------------
