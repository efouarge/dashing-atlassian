require 'dashing'
require 'dotenv'
require 'json'
require 'oauth'
require 'omniauth/strategies/bitbucket'
require 'rest-client'

require './settings.rb'

configure do
  Dotenv.load

  use Rack::Session::Cookie
# @TODO: set Rack::Session::Cookie

  authentication = Potherca::Dashing::AuthenticationTokens.new(ENV)
  team = Potherca::Dashing.settings[:bitbucket][:team]

  set :auth_token, authentication.tokens['DASHING_AUTH_TOKEN']

  use OmniAuth::Builder do
    provider :bitbucket, ENV['BITBUCKET_KEY'], ENV['BITBUCKET_SECRET']
  end

  OmniAuth.config.on_failure = Proc.new { |env|
    OmniAuth::FailureEndpoint.new(env).redirect_to_failure
  }

  helpers do
    def protected!
      # This method is run before accessing any resource.
      # @SEE: https://github.com/Shopify/dashing/wiki/How-to%3A-Add-authentication
      redirect '/auth/bitbucket' unless session[:user_id]
    end
  end

  get '/auth/:name/callback' do
    #@FIXME: Handle 400/500 errors from BitBucket
    response_json = RestClient.get "https://bitbucket.org/api/2.0/teams/#{team}/members",
      accept: :json,
      params: {}

    response = JSON.parse(response_json)
    users = response['values']

    user_names = {}
    users.each do |user|
      user_names[user['uuid']] = user['username']
    end

    auth = request.env['omniauth.auth']

    if auth && user_names.has_value?(auth.uid)
      session[:user_id] = auth.uid
      session[:jira_token] = 'foo'
      url = '/'
    elsif !user_names.has_value?(auth.uid)
      url = "/auth/failure?message=not_in_team&strategy=bitbucket"
    else
      url = '/auth/failure'
    end

    redirect url
  end

  get '/auth/failure' do
    "Authentication Failed -  #{params['strategy']} : #{params['message']}"
  end

# get '/auth/bitbucket/' do
#   <<-HTML
#   <a href='/auth/bitbucket'>Sign in with Bitbucket</a>
#   HTML
# end

end

map Sinatra::Application.assets_prefix do
  run Sinatra::Application.sprockets
end

run Sinatra::Application

#EOF
